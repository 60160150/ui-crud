const userService = {
  userList: [{
    ลำดับ: 1,
    ชื่อบริษัท: 'ClickNext',
    ประเภท: 'วิชาการ',
    รายละเอียด: 'xxxxxxxxxxxxxxxxxxxxxxxx'
  },
  {
    ลำดับ: 2,
    ชื่อบริษัท: 'CDG',
    ประเภท: 'วิชาการ',
    รายละเอียด: 'yyyyyyyyyyyyyyyyyyyyyyyy'
  },
  {
    ลำดับ: 3,
    ชื่อบริษัท: 'G-able',
    ประเภท: 'เตรียมความพร้อม',
    รายละเอียด: 'yyyyyyyyyyyyyyyyyyyyyyyy'
  }

  ],
  lastId: 4,
  addUser (user) {
    user.ลำดับ = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.ลำดับ === user.ลำดับ)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.ลำดับ === user.ลำดับ)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
