import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/main',
    name: 'main',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/main.vue')
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/educate',
    name: 'educate',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/educate.vue')

  },
  {
    path: '/declareCoop',
    name: 'declareCoop',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/declareCoop.vue')

  },
  {
    path: '/declarePermission',
    name: 'declarePermission',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/declarePermission.vue')

  },
  {
    path: '/interviewEligibility',
    name: 'interviewEligibility',
    component: () => import(/* webpackChunkName: "about" */ '../views/student/listSudent.vue')

  },
  {
    path: '/ShowUserPassCE',
    name: 'ShowUserPassCE',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/ShowUserPassCE.vue')

  },
  {
    path: '/company',
    name: 'company',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/company.vue')

  },
  {
    path: '/StudentsCooperativeEducation',
    name: 'StudentsCooperativeEducation',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/StudentsCooperativeEducation.vue')

  },
  {
    path: '/Document',
    name: 'Document',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/Document.vue')

  },
  {
    path: '/mainStudent',
    name: 'mainStudent',
    component: () => import(/* webpackChunkName: "about" */ '../views/student/main.vue')

  },
  {
    path: '/mainNologin',
    name: 'mainNologin',
    component: () => import(/* webpackChunkName: "about" */ '../views/noLogin/main.vue')

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
